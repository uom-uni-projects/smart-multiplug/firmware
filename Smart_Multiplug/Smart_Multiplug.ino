#include <IRremote.h>
int RecvPin = 13;
IRrecv irrecv(RecvPin);
decode_results results;

int OnBulb=11;
int UsbPower=12;
int PlugBase=5;

char data = 0;

int UsbCnt=0, PlugCnt=0, BlinkTime=0, Switch=0 ; // count for USB plugbase Blinker

void setup() {
  Serial.begin(9600);

  pinMode(OnBulb,OUTPUT);
  pinMode(UsbPower,OUTPUT);
  pinMode(PlugBase,OUTPUT);
  
  irrecv.enableIRIn();

  
 
}

void loop() {
 BlinkBulb();  // blink on light
 IrBulb();   // read ir values
//delay(500);
 if(Serial.available()>0){
  BluetoothTerminal(); //read serial monitor
 }

  Switch=analogRead(A0);
  Serial.println(Switch);
  if (Switch>1000){
    PlugCnt=1;// include switch 
 }
 
Switching(); // on or off ports

}

void IrBulb(){
  if (irrecv.decode(&results)) {
    irrecv.resume();
  }
Serial.println(results.value, HEX);
  switch(results.value){
  case 0x1FE48B7: 
    UsbCnt=1;
    break;
  case 0x1FE58A7:
    UsbCnt=0;
    break; 
  case 0x1FE807F: 
    PlugCnt=1;
    break;
  case 0x1FE40BF:
    PlugCnt=0;
    break;
  }
}



void BlinkBulb(){
   
        digitalWrite(OnBulb,HIGH);  
//        delay(100);
//
//  
//         digitalWrite(OnBulb,LOW);  
//          delay(100);
//     
}



void BluetoothTerminal(){
  data = Serial.read();
  Serial.println(data);               
    if(data == 'A')             
      UsbCnt=1;   
    else if(data == 'a')       
      UsbCnt=0;   
    if(data == 'B')             
      PlugCnt=1;  
    else if(data == 'b')
      PlugCnt=0;
}


void Switching(){
  if (UsbCnt==1){
    digitalWrite(UsbPower,HIGH);
  }
  else if (UsbCnt==0){
    digitalWrite(UsbPower,LOW);
  }
  if (PlugCnt==1){
  digitalWrite(PlugBase,HIGH);
  }
  else if (PlugCnt==0){
  digitalWrite(PlugBase,LOW);
  }
  
}

